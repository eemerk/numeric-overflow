package app;

import java.util.Arrays;

public class Main {

    private int threshold = 1000;

    public static void main(String[] args) {

        Main app = new Main();

        int amount = 999;
        if (!app.approval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        amount = 2000;
        if (app.approval(amount)) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(int amount){
       if(amount >= threshold) {
           return true;
       }
       return false;
   }

}
